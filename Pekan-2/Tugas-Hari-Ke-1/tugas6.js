//soal1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];

var objectDaftarPeserta = {
  nama: "Asep",
  "jenis kelamin": "laki-laki",
  hobi: "baca buku",
  "tahun lahir": 1992,
};

console.log(objectDaftarPeserta);

//soal2
var fruit = [
  { nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
  { nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  { nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000 },
];

console.log(fruit[0]);

//soal3
var dataFilm = [];

var objectFilm = {
  nama: "The Eternals",
  durasi: 180,
  genre: "sci-fi",
  tahun: 2021,
};

var tambahFilm = function (add) {
  var arr = dataFilm.push(add);
  console.log(dataFilm);
  return arr;
};

tambahFilm(objectFilm);

//soal4
class Animal {
  constructor(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }
  set name(x) {
    this._name = x;
  }
}

var sheep = new Animal("shaun");
sheep.legs = 4;
sheep.cold_blooded = false;

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

class Ape extends Animal {
  constructor(name) {
    super(name);
  }
  yell() {
    var show = console.log(this._name + " \n" + "Auooo");
    return show;
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name);
  }
  jump() {
    var show = console.log(this._name + " \n" + "hop hop");
    return show;
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//soal5
class Clock {
  constructor({ template }) {
    var timer;

    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = "0" + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = "0" + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = "0" + secs;

      var output = template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);

      console.log(output);
    }

    this.stop = function () {
      clearInterval(timer);
    };

    this.start = function () {
      render();
      timer = setInterval(render, 1000);
    };
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
