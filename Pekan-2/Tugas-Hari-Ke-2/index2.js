var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

//soal2
// Lanjutkan code untuk menjalankan function readBooksPromise
function read(waktu, i) {
  readBooksPromise(waktu, books[i])
    .then(function (baca) {
      readBooksPromise(baca, books[1]).then(function (baca) {
        readBooksPromise(baca, books[2]);
      });
    })
    .catch(function (sisaWaktu) {
      console.log(sisaWaktu);
    });
}
read(10000, 0);
