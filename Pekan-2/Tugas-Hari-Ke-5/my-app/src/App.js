import React, { Component } from "react";

import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <h1 style={{ textAlign: "center" }}>Form Pembelian Buah</h1>

          <div className="pelanggan">
            <label style={{ marginRight: "20px" }}>
              <b>Nama Pelanggan</b>
            </label>
            <input type="text" /> <br />
          </div>

          <div className="box">
            <div className="labelBox">
              <label style={{ float: "left" }}>
                <b>Daftar Item</b>
              </label>
            </div>

            <div className="checkBox">
              <input type="checkbox" />
              <label for="vehicle1"> Semangka</label>
              <br />
              <input type="checkbox" />
              <label for="vehicle1"> Jeruk</label>
              <br />
              <input type="checkbox" />
              <label for="vehicle1"> Nanas</label>
              <br />
              <input type="checkbox" />
              <label for="vehicle1"> Salak</label>
              <br />
              <input type="checkbox" />
              <label for="vehicle1"> Anggur</label>
            </div>
          </div>
          <input
            type="submit"
            value="Kirim"
            style={{ borderRadius: "10px;", fontSize: "15px;" }}
          />
        </div>
      </div>
    );
  }
}

export default App;
