//soal1
const kelilingLingkaran = (diameter) => {
  let keliling = 3.14 * diameter;
  return keliling;
};

const luasLingkaran = (diameter) => {
  let keliling = (((1 / 4) * 22) / 7) * diameter * diameter;
  return keliling;
};
console.log(kelilingLingkaran(10));
console.log(luasLingkaran(14));

//soal2

let kalimat = "";

const addKalimat = (str1, str2, str3, str4, str5) => {
  let kata = (kalimat = `${str1} ${str2} ${str3} ${str4} ${str5}`);
  console.log(kalimat);
  return kata;
};

addKalimat("saya", "adalah", "seorang", "frontend", "developer");

//soal3
class Book {
  constructor(name, totalPage, price) {
    this._name = name;
    this._totalPage = totalPage;
    this._price = price;
  }
  get name() {
    return this._name;
  }
  get totalPage() {
    return this._totalPage;
  }
  get price() {
    return this._price;
  }
  show() {
    console.log(`Book: ${this.name}`);
    console.log(`Page: ${this.totalPage}`);
    console.log(`Price: ${this.price}`);
  }
}
class Komik extends Book {
  constructor(name, totalPage, price, isColorful) {
    super(name, totalPage, price);
    this._isColorful = isColorful;
  }
  show() {
    console.log(`Book: ${this.name}`);
    console.log(`Page: ${this.totalPage}`);
    console.log(`Price: ${this.price}`);
    console.log(`Price: ${this._isColorful}`);
  }
}

let read = new Book("LOTR", 100, 10000);
let read2 = new Komik("Haikyuu", 400, 20000, false);
read.show();
console.log("");
read2.show();
