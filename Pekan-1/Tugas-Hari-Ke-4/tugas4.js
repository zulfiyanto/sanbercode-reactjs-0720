//soal1
var i = 1;
var a = 20;
console.log("LOOPING PERTAMA");
while (i <= 20) {
  if (i % 2 === 0) {
    console.log(i + " - I love coding");
  }
  i++;
}
console.log("LOOPING KEDUA");
while (a >= 1) {
  if (a % 2 === 0) {
    console.log(a + " - I will become a frontend developer");
  }
  a--;
}

console.log(" ");

//soal2
var angka = 1;
for (angka; angka <= 20; angka++) {
  if (angka % 2 === 0) {
    console.log(angka + " Berkualitas");
  } else if (angka % 3 === 0) {
    console.log(angka + " I love coding");
  } else {
    console.log(angka + " Santai");
  }
}

//soal3
var patern = "";
for (var i = 0; i < 7; i++) {
  for (var a = 0; a <= i; a++) {
    patern = patern + "#";
  }
  patern = patern + "\n";
}
console.log(patern);

console.log(" ");

//soal4
var kalimat = "saya sangat senang belajar javascript";
var arr = kalimat.split(" ");
console.log(arr);

console.log(" ");

//soal5
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];
daftarBuah.sort();
console.log(daftarBuah.join("\n"));
