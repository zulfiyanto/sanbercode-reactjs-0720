//soal1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(
  kataPertama +
    " " +
    kataKedua.charAt(0).toUpperCase() +
    kataKedua.slice(1) +
    " " +
    kataKetiga +
    " " +
    kataKeempat.toUpperCase()
);

//soal2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var jumlah =
  parseInt(kataPertama) +
  parseInt(kataKedua) +
  parseInt(kataKetiga) +
  parseInt(kataKeempat);
console.log(jumlah);

//soal3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);

//soal4
var nilai = 40;

if (nilai >= 80) {
  console.log("A");
} else if (nilai >= 70 && nilai < 80) {
  console.log("B");
} else if (nilai >= 60 && nilai < 70) {
  console.log("C");
} else if (nilai >= 50 && niali < 60) {
  console.log("D");
} else if (nilai < 50) {
  console.log("E");
}

//soal5
var tanggal = 12;
var bulan = 3;
var tahun = 1994;

switch (bulan) {
  case 1:
    console.log("12 Januari 1994");
    break;
  case 2:
    console.log("12 Februari 1994");
    break;
  case 3:
    console.log("12 Maret 1994");
    break;

  default:
    break;
}
