//soal1
function halo() {
  var string = "Halo Sanbers!";
  return string;
}

console.log(halo()); // "Halo Sanbers!"

console.log(" ");

//soal2
function kalikan(int1, int2) {
  return int1 * int2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

console.log(" ");

//soal3
function introduce(name, age, address, hobby) {
  var biodata =
    "Nama saya " +
    name +
    ", umur saya " +
    age +
    " tahun, alamat saya di " +
    address +
    ", dan saya punya hobby yaitu " +
    hobby +
    "!";
  return biodata;
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"
